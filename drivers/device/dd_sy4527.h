/********************************************************************\

  Name:         dd_sy4527.h
  Created by:   based on null.h / Stefan Ritt

  Contents:     Device driver function declarations for SY4527 device

  $Id$

\********************************************************************/

INT dd_sy4527(INT cmd, ...);
